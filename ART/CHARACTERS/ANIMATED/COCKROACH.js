(function (lib, img, cjs, ss, an) {

var p; // shortcut to reference prototypes
lib.ssMetadata = [
		{name:"COCKROACH_atlas_", frames: [[0,1583,262,435],[342,1154,316,386],[506,437,166,304],[0,1154,340,427],[342,1542,381,316],[506,743,117,307],[506,0,262,435],[0,0,504,1152]]}
];


// symbols:



(lib.BODY = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(0);
}).prototype = p = new cjs.Sprite();



(lib.LEFTANTENNA = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(1);
}).prototype = p = new cjs.Sprite();



(lib.LEFTLEG = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(2);
}).prototype = p = new cjs.Sprite();



(lib.LEFTWING = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(3);
}).prototype = p = new cjs.Sprite();



(lib.RIGHTANTENNA = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(4);
}).prototype = p = new cjs.Sprite();



(lib.RIGHTLEG = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(5);
}).prototype = p = new cjs.Sprite();



(lib.RIGHTWING = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(6);
}).prototype = p = new cjs.Sprite();



(lib.SPOON = function() {
	this.spriteSheet = ss["COCKROACH_atlas_"];
	this.gotoAndStop(7);
}).prototype = p = new cjs.Sprite();
// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.RIGHTWING_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.LEFTWING();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.535,0.488);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.RIGHTWING_1, new cjs.Rectangle(0,0,182,208.5), null);


(lib.RIGHTLEG_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.LEFTLEG();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.566,0.566);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.RIGHTLEG_1, new cjs.Rectangle(0,0,93.9,172), null);


(lib.RIGHTANTENA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.LEFTANTENNA();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.367,0.367);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.RIGHTANTENA, new cjs.Rectangle(0,0,116,141.7), null);


(lib.LEFTWING_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.RIGHTWING();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.504,0.504);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.LEFTWING_1, new cjs.Rectangle(0,0,132,219.2), null);


(lib.LEFTLEG_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.RIGHTLEG();
	this.instance.parent = this;
	this.instance.setTransform(0,0,0.556,0.556);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.LEFTLEG_1, new cjs.Rectangle(0,0,65,170.6), null);


(lib.LEFTANTENA = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.RIGHTANTENNA();
	this.instance.parent = this;
	this.instance.setTransform(0,40.6,0.412,0.412,-15);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.LEFTANTENA, new cjs.Rectangle(0,0,185.3,166.3), null);


(lib.BODY_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.BODY();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.BODY_1, new cjs.Rectangle(0,0,262,435), null);


// stage content:
(lib.COCKROACH = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// RIGHT WING
	this.instance = new lib.RIGHTWING_1();
	this.instance.parent = this;
	this.instance.setTransform(313.3,134.2,1,1,0,0,0,56.3,15.2);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regX:56.4,rotation:-4.2,x:313.4},0).wait(3).to({regX:56.3,rotation:0,x:313.3},0).wait(3));

	// LEFT ANTENA
	this.instance_1 = new lib.LEFTANTENA();
	this.instance_1.parent = this;
	this.instance_1.setTransform(232.8,95.7,1,1,0,0,0,92.6,83.2);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(1).to({regX:134.4,regY:67.3,rotation:-3.5,x:274.6,y:79.8},0).wait(3).to({regX:92.6,regY:83.2,rotation:0,x:232.8,y:95.7},0).wait(3));

	// RIGHT ANTENA
	this.instance_2 = new lib.RIGHTANTENA();
	this.instance_2.parent = this;
	this.instance_2.setTransform(336,102.9,1,1,0,0,0,58,70.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(1).to({regX:16.2,regY:56.7,rotation:-4.2,x:294.3,y:88.7},0).wait(3).to({regX:58,regY:70.9,rotation:0,x:336,y:102.9},0).wait(3));

	// Layer 10
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9F5434").s().p("AhTIkQgLgHgEgOQgEgOAFgOQAFgTAagYQAZgYAdgSQAMgIAMgFIAAgBIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgFIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIAAgGIACgGIAAgFIAAgGIgCgGIACgGIAAgGIACgGIAGgGIAGgEIAEgCIAJAAIAAgCIAOAEIAIAGIAEAGIACAGIAEAGIACAGIACAGIAAAFIAAAGIgCAGIgEAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAGIAAAFIAAAGIAAAGIAAAGIAAAEQATAFAGAIQAIALgFAWQgFAcgJAPQgSAfgmAQQgaAMgtAFIgNABQgTAAgJgGg");
	this.shape.setTransform(314.1,241.6);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(2).to({y:230.4},0).wait(2).to({y:241.6},0).wait(3));

	// BODY
	this.instance_3 = new lib.BODY_1();
	this.instance_3.parent = this;
	this.instance_3.setTransform(285.3,186.5,0.573,0.573,0,0,0,130.9,217.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(7));

	// Layer 11
	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#4F2D29").s().p("AkmHUQgPgBgMgKQgMgLAAgOQABgNAPgRQAagdAngfQAVgQA0gkQAXgQAPABIACgEQATgdAgg9QBhi4BNiGQBiiqBfiKQANgTAMgCQAMgCAJALQAJAKAAANQAAALgGANIgOAVQhnCWhsC+QhPCKhuDVQgKAWgLAJIgDABQgIAWgcAZQgpAlg1AfQggATgVAAIgBAAg");
	this.shape_1.setTransform(219.4,202.8);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("#4F2D29").s().p("AkmHUQgPgBgMgKQgMgLAAgOQABgNAPgRQAagdAngfQAVgQA0gkQAXgQAPABIACgEQATgdAgg9QBhi4BNiGQBiiqBfiKQANgTAMgCQAMgCAJALQAJAKAAANQAAALgGANIgOAVQhnCWhsC+QhPCKhuDVQgKAWgLAJIgCACQgJAVgcAZQgpAlg1AfQggATgVAAIgBAAg");
	this.shape_2.setTransform(215.4,206.8);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1}]}).to({state:[{t:this.shape_2,p:{x:215.4,y:206.8}}]},1).to({state:[{t:this.shape_2,p:{x:227.3,y:197.3}}]},3).wait(3));

	// Layer 8
	this.instance_4 = new lib.SPOON();
	this.instance_4.parent = this;
	this.instance_4.setTransform(120.7,51.7,0.23,0.23,-5.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(1).to({rotation:-6.5,x:110.8,y:47.7},0).wait(3).to({rotation:-15,x:97.4,y:62},0).wait(3));

	// LEFT LEG
	this.instance_5 = new lib.LEFTLEG_1();
	this.instance_5.parent = this;
	this.instance_5.setTransform(271.5,281.4,1,1,0,0,0,32.5,73.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(4).to({y:259},0).wait(3));

	// RIGHT LEG
	this.instance_6 = new lib.RIGHTLEG_1();
	this.instance_6.parent = this;
	this.instance_6.setTransform(298,277,1,1,0,0,0,47,86);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1).to({y:245},0).wait(3).to({y:277},0).wait(3));

	// LEFT WING
	this.instance_7 = new lib.LEFTWING_1();
	this.instance_7.parent = this;
	this.instance_7.setTransform(247,217.6,1,1,0,0,0,66,109.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({regX:66.1,regY:45.8,rotation:7.7,x:247.1,y:153.9},0).wait(3).to({regX:57.3,regY:33.9,rotation:0,x:238.3,y:141.9},0).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(395.7,212.5,318.4,366.1);
// library properties:
lib.properties = {
	width: 550,
	height: 400,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/COCKROACH_atlas_.png?1541638777890", id:"COCKROACH_atlas_"}
	],
	preloads: []
};




})(lib = lib||{}, images = images||{}, createjs = createjs||{}, ss = ss||{}, AdobeAn = AdobeAn||{});
var lib, images, createjs, ss, AdobeAn;