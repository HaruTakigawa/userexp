﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour
{
    public float MaxHp;
    public float CurrentHP;
    public Image HealthBar;
    public UnityEvent OnDeath;
    public List<Sprite> healthSprites;


    void Start()
    {
        if (OnDeath == null) OnDeath = new UnityEvent();
        CurrentHP = MaxHp;
    }

    void Update()
    {
        if (gameObject.layer == 10)
        {
            if (CurrentHP != HealthBar.fillAmount)
            {
                HealthBar.fillAmount = CurrentHP / MaxHp;
            }
        }

        if (gameObject.layer == 8)
        {
            if (CurrentHP <= 100 && CurrentHP > 90)
            {
                HealthBar.sprite = healthSprites[9];
            }
            else if (CurrentHP <= 90 && CurrentHP > 80)
            {
                HealthBar.sprite = healthSprites[8];
            }
            else if (CurrentHP <= 80 && CurrentHP > 70)
            {
                HealthBar.sprite = healthSprites[7];
            }
            else if (CurrentHP <= 70 && CurrentHP > 60)
            {
                HealthBar.sprite = healthSprites[6];
            }
            else if (CurrentHP <= 60 && CurrentHP > 50)
            {
                HealthBar.sprite = healthSprites[5];
            }
            else if (CurrentHP <= 50 && CurrentHP > 40)
            {
                HealthBar.sprite = healthSprites[4];
            }
            else if (CurrentHP <= 40 && CurrentHP > 30)
            {
                HealthBar.sprite = healthSprites[3];
            }
            else if (CurrentHP <= 30 && CurrentHP > 20)
            {
                HealthBar.sprite = healthSprites[2];
            }
            else if (CurrentHP <= 20 && CurrentHP > 10)
            {
                HealthBar.sprite = healthSprites[1];
            }
            else if (CurrentHP <= 10 && CurrentHP > 0)
            {
                HealthBar.sprite = healthSprites[0];
            }
        }
    }

    public void TakeDamage(float damage)
    {
       
        CurrentHP -= damage;


        // If entity is dead, remove
        if (CurrentHP <= 0)
        {
            //Player layer
            if (gameObject.layer == 8)
            {
                gameObject.SetActive(false);
                SceneManager.LoadScene("GameOver");
            }
            // Enemy Layer
            else if(gameObject.layer == 10)
            {         
              OnDeath.Invoke(); // Broadcast to all listeners
            }
            else
            {
                //do nothing
            }
        }
    }

    public void TakeHealing(float healValue)
    {
        CurrentHP += healValue;

        if (CurrentHP >= MaxHp)
        {
            CurrentHP = MaxHp;
        }
    }

    public void Destroy()
    {
        this.gameObject.SetActive(false);
    }
}
