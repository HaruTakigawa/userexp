﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollow : MonoBehaviour {

    public Animator animator;

    public float speed;
    public Transform target;
    public float chaseRange;
    /*
    public float speed;
    public float stoppingDistance;
    public float retreatDistance;
    */
  //  public GameObject player;

   // private Transform target;

	// Use this for initialization
	void Start () {

        //target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        //target = player.transform;

	}
	
	// Update is called once per frame

	void Update () {
        /*
        if (Vector2.Distance(transform.position, target.position) >  stoppingDistance ){
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
        }
        else if(Vector2.Distance(transform.position, target.position) < stoppingDistance && Vector2.Distance(transform.position, target.position) > retreatDistance){

            transform.position = this.transform.position;

        }
        else if(Vector2.Distance(transform.position, target.position) < retreatDistance){

            transform.position = Vector2.MoveTowards(transform.position, target.position, -speed * Time.deltaTime);
        }
        */
        Chase();


    }

     //Chasing Player AI
     void Chase()
    {
        float distanceToTarget = Vector3.Distance(transform.position, target.position);
        if(distanceToTarget < chaseRange)
        {

            
            //Start chasing the target - turn and move towards the target
            Vector3 targetDir = target.position - transform.position;
            float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 180);
            transform.position = Vector2.MoveTowards(transform.position, target.position, speed * Time.deltaTime);
           // transform.Translate(Vector3.up * Time.deltaTime * speed);
        }
    }
}
