﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public Animator animator;

    public float speed;
    public float currentSpeed;
    public float clamp;
    public bool m_FacingRight = true;
    private Rigidbody2D rb;
    private Vector2 moveVelocity;
    float horizontalMove = 0f;
    float verticalMove = 0f;
    public float health;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update () {

        if (Mathf.Abs(this.transform.position.y) <= clamp)
        {
            StartCoroutine(Clamp());
        }

        horizontalMove = Input.GetAxisRaw("Horizontal") * speed;
        verticalMove = Input.GetAxisRaw("Vertical") * speed;

        animator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        animator.SetFloat("SpeedY", Mathf.Abs(verticalMove));

        Vector2 moveInput = new Vector2(horizontalMove , Input.GetAxisRaw("Vertical"));
        moveVelocity = moveInput.normalized * speed;

        

        if (horizontalMove > 0 && !m_FacingRight)
        {
            Flip();
        }
        else if (horizontalMove < 0 && m_FacingRight)
        {
            Flip();
        }

    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + moveVelocity * Time.fixedDeltaTime);

    }


    void Flip()
    {
        m_FacingRight = !m_FacingRight;

        transform.Rotate(0f, 180f, 0f);

    }

   public void TakeDamage(int damage)
    {
        health -= damage;
        if(health <= 0)
        {
            Destroy(gameObject);
        }
    }

    IEnumerator Clamp()
    {
        speed *= -1;

        yield return new WaitForSeconds(1);

        speed = currentSpeed;


    }

}





    /*if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector2.right* speed * Time.deltaTime);
            if (speed > 0 && !m_FacingRight)
            {
                Flip();
            }
           
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector2.left* speed * Time.deltaTime);
            if (speed > 0 && m_FacingRight)
            {
                Flip();
              
            }
          

        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector2.up* speed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector2.down* speed * Time.deltaTime);
        }
        */