﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public int speed = 100;
    public float damage = 10f;
    public float bulletRange = 0.5f;
    public Rigidbody2D rb;
    public GameObject rangeAttack;
    // Use this for initialization
    void Start()
    {
        rb.velocity = transform.right * speed * Time.deltaTime;
        //rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, bulletRange);
    }

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.gameObject.CompareTag("Enemy"))
        {          
            Health health = hitInfo.GetComponent<Health>();
            if (health != null)
            {
                PlayerRangeAtk stat = rangeAttack.GetComponent<PlayerRangeAtk>();
                //stat.rangeDamage = 10f;
                health.TakeDamage(stat.rangeDamage);
            }
            Destroy(gameObject);
            
         
          
        }
    }


}