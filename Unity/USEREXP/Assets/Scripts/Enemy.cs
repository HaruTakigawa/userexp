﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Enemy : MonoBehaviour {

    public Animator animator;
    public Animator transitionAnim;
    public string sceneName;


    public float health = 100f;
    public float speed;
    public float currentSpeed;
    private float dazedTime;
    public float startDazedTime;
    public float chanceToDrop;
    public float dropPercentage = 0.45f;


    public int whatToDrop = 0;

    public bool drops;
    public GameObject[] lootDrop;

    private float timeBtwShots;
    public float startTimeBtwShots;

    public GameObject projectile;
    public Transform player;
    public EnemyManager manager;
    public EnemyCounter counter;
    public bool boss = false;
    public bool bossIsDead;

    //Melee Attack
    public float attackRange;
    public int damage;
    private float lastAttackTime;
    public float attackDelay;

    public bool m_FacingRight = true;

    // Update is called once per frame
    void Update () {

        if (this.transform.position.y <= -1.04 || this.transform.position.y <= -4.89)
        {
            StartCoroutine(Clamp());
        }

        if (dazedTime <= 0)
        {
            speed = 0;
        }
        else
        {         
            speed = 0;
           
            dazedTime -= Time.deltaTime;
        }

        transform.Translate(Vector2.left * speed * Time.deltaTime);

        if (player.position.x > gameObject.transform.position.x && m_FacingRight)
            Flip();

        else if (player.position.x < gameObject.transform.position.x && !m_FacingRight)
            Flip();

        //Shooting();
        Melee();

	}

    public void TakeDamage(float damage)
    {
        dazedTime = startDazedTime;
        health -= damage;
        Debug.Log("damage TAKEN!");
        if(health <= 0)
        {
            Die();
           
        }
    }

    void Die()
    {
        manager.GetComponent<EnemyManager>().enemies.Remove(this);
        counter.GetComponent<EnemyCounter>().AddToTotal(1);
        gameObject.SetActive(false);
        
        chanceToDrop = Random.value;
        if (chanceToDrop < dropPercentage)
        {
            drops = true;
            whatToDrop = Random.Range(0, 1);
            Instantiate(lootDrop[whatToDrop], transform.position, transform.rotation);
            
            
        }
        if(boss == true)
        {
            SceneManager.LoadScene(sceneName);
        }
        
    }


     void Shooting()
    {
        if (timeBtwShots <= 0)
        {
            Instantiate(projectile, transform.position, Quaternion.identity);
            timeBtwShots = startTimeBtwShots;
        } 
        else
        {
            timeBtwShots -= Time.deltaTime;
        }
    }

   //Melee Attack

   //Check the distance between AI and player to see if the polayer is close enough to attack
   void Melee()
    {
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);
        if(distanceToPlayer < attackRange)
        {
            Debug.Log("Attack Player");
            animator.SetTrigger("Attack");
            //Check To see if enough time has passed since we last attacked
            if(Time.time > lastAttackTime + attackDelay)
            {
                Health health = player.GetComponent<Health>();
                if (health != null)
                {
                    health.TakeDamage(damage);
                }
                
                //Record the time we attacked
                lastAttackTime = Time.time;
            }
        }

    }


    IEnumerator Clamp()
    {
        speed *= -1;

        yield return new WaitForSeconds(1);

        speed = currentSpeed;


    }

    IEnumerator LoadWin()
    {
        transitionAnim.SetTrigger("end");
        yield return new WaitForSeconds(1.5f);
        SceneManager.LoadScene(sceneName);
    }
    
    void Flip()
    {
        m_FacingRight = !m_FacingRight;

        transform.Rotate(0f, 180f, 0f);
    }
}
