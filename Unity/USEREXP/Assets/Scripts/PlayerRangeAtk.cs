﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRangeAtk : MonoBehaviour
{
    public Animator animator;

    public float fireRate;
    public Transform firePoint;
    public Transform muzzleFlashPrefab;
    public GameObject bulletPrefab;
    public float rangeDamage;
    public float timeToFire;

    // Update is called once per frame
    void Update()
    {
        if (fireRate == 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                animator.SetTrigger("RangeAttack");
                Effect();
                Shoot();
            }
                  
        }
        else
        {
            if (Input.GetButton("Fire1") && Time.time > timeToFire)
            {
                animator.SetTrigger("RangeAttack");
                timeToFire = Time.time + 1 / fireRate;
                Effect();
                Shoot();
            }
        }
    }

    void Shoot()
    {
        // shooting logic
        Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);

    }

    void Effect()
    {
        Transform clone = Instantiate(muzzleFlashPrefab, firePoint.position, firePoint.rotation) as Transform;
        clone.parent = firePoint;
        float size = Random.Range(1f, 2f);
        clone.localScale = new Vector3(size, size, size);
        Destroy(clone.gameObject, 0.02f);

    }
}
