﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyCounter : MonoBehaviour
{
    public int Total;
    public GameObject ExitPoint;
    [SerializeField] public static int CurrentTotal;

    // Use this for initialization
    void Start ()
    {
        CurrentTotal = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (CurrentTotal >= Total)
        {
            ExitPoint.SetActive(true);
            Debug.Log("Exit");
        }
        Debug.Log(CurrentTotal);
    }

    public void AddToTotal(int kill)
    {
        CurrentTotal += kill;

    } 
}
