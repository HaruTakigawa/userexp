﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackMelee : MonoBehaviour {

    public Animator animator;

    private float timeBtwAttack;
    public float startTimeBtwAttack;

    public Transform attackPos;
    public LayerMask whatIsEnemies;
    
    public float attackRange;
    //public float attackRangeY;
    public int damage;

	
	// Update is called once per frame
	void Update ()
    {
		
        if(timeBtwAttack <= 0)
        {
            if (Input.GetButton("Fire2"))
            {
                animator.SetTrigger("MeleeAttack");
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    Debug.Log("Nep");
                    Health health = enemiesToDamage[i].GetComponent<Health>();
                    health.TakeDamage(damage);
                }
            }
            timeBtwAttack = startTimeBtwAttack;
        }
        
        else
        {
            timeBtwAttack -= Time.deltaTime;
        }

        

    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
}
