﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class PowerUp : MonoBehaviour
{
    public float decreaseSpeed = 1.0f;

    public Image barTime;
    public GameObject bar;
    bool Activebar = false;
    public float multiplier = 1.4f;
    public float duration = 10.0f;
    public float wasabiTime;
    public float soyTime;
    public bool isWasabiActive;
    public bool isSoyActive;

    public UnityEvent powerUpUI;
    public GameObject pickupEffect;
    public List<Sprite> soySprites;
    public List<Sprite> wasabiSprites;

    void Start()
    {
        soySprites.Reverse();
        wasabiSprites.Reverse();
        wasabiTime = duration;
        soyTime = duration;
        isWasabiActive = false;
        isSoyActive = false;
    }


    void Update()
    {
        if (isWasabiActive == true)
        {
            if (wasabiTime <= 10 && wasabiTime > 9)
            {
                barTime.sprite = wasabiSprites[0];
            }
            else if (wasabiTime <= 9 && wasabiTime > 8)
            {
                barTime.sprite = wasabiSprites[1];
            }
            else if (wasabiTime <= 8 && wasabiTime > 7)
            {
                barTime.sprite = wasabiSprites[2];
            }
            else if (wasabiTime <= 7 && wasabiTime > 6)
            {
                barTime.sprite = wasabiSprites[3];
            }
            else if (wasabiTime <= 6 && wasabiTime > 5)
            {
                barTime.sprite = wasabiSprites[4];
            }
            else if (wasabiTime <= 5 && wasabiTime > 4)
            {
                barTime.sprite = wasabiSprites[5];
            }
            else if (wasabiTime <= 4 && wasabiTime > 3)
            {
                barTime.sprite = wasabiSprites[6];
            }
            else if (wasabiTime <= 3 && wasabiTime > 2)
            {
                barTime.sprite = wasabiSprites[7];
            }
            else if (wasabiTime <= 2 && wasabiTime > 1)
            {
                barTime.sprite = wasabiSprites[8];
            }
            else if (wasabiTime <= 1 && wasabiTime > 0)
            {
                barTime.sprite = wasabiSprites[9];
            }
        }

        if (isSoyActive == true)
        {
            if (soyTime <= 10 && soyTime > 9)
            {
                barTime.sprite = soySprites[0];
            }
            else if (soyTime <= 9 && soyTime > 8)
            {
                barTime.sprite = soySprites[1];
            }
            else if (soyTime <= 8 && soyTime > 7)
            {
                barTime.sprite = soySprites[2];
            }
            else if (soyTime <= 7 && soyTime > 6)
            {
                barTime.sprite = soySprites[3];
            }
            else if (soyTime <= 6 && soyTime > 5)
            {
                barTime.sprite = soySprites[4];
            }
            else if (soyTime <= 5 && soyTime > 4)
            {
                barTime.sprite = soySprites[5];
            }
            else if (soyTime <= 4 && soyTime > 3)
            {
                barTime.sprite = soySprites[6];
            }
            else if (soyTime <= 3 && soyTime > 2)
            {
                barTime.sprite = soySprites[7];
            }
            else if (soyTime <= 2 && soyTime > 1)
            {
                barTime.sprite = soySprites[8];
            }
            else if (soyTime <= 1 && soyTime > 0)
            {
                barTime.sprite = soySprites[9];
            }
        }
        
    }

    void WasabiCountdown()
    {
        if (--wasabiTime == 0) CancelInvoke("WasabiCountdown");
    }

    void SoyCountdown()
    {
        if (--soyTime == 0) CancelInvoke("SoyCountdown");
    }


    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (gameObject.tag == "Wasabi")
            {
            StartCoroutine(WasabiPickup(other));
            powerUpUI.Invoke();
            }
            else
            {
                StartCoroutine(SoyPickup(other));
            }
        }
    }


    IEnumerator WasabiPickup(Collider2D player)
    {
        isWasabiActive = true;
        InvokeRepeating("WasabiCountdown", 1.0f, 1.0f);
        // Spawm a cool effect
        Instantiate(pickupEffect, transform.position, transform.rotation);

        bar.SetActive(true);
        Activebar = true;

        //Apply effect to the player

        PlayerRangeAtk damageDealt = player.GetComponent<PlayerRangeAtk>();
            damageDealt.rangeDamage *= multiplier;
        
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        while (true)
        {
            if (wasabiTime <= 0)
            {
                //Reverse the effect on our player
                damageDealt.rangeDamage /= multiplier;
                bar.SetActive(false);
                Activebar = false;

                //Remove power up object
                Instantiate(pickupEffect, player.transform.position, player.transform.rotation);
                Destroy(gameObject);

                break;
            }

            yield return new WaitForSeconds(1);
        }
    }


    IEnumerator SoyPickup(Collider2D player)
    {
        isSoyActive = true;
        InvokeRepeating("SoyCountdown", 1.0f, 1.0f);
        // Spawm a cool effect
        Instantiate(pickupEffect, transform.position, transform.rotation);
        bar.SetActive(true);
        Activebar = true;

        //Apply effect to the player

        PlayerMovement boost = player.GetComponent<PlayerMovement>();
        boost.speed *= multiplier;

        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;

        while (true)
        {
            if (soyTime <= 0)
            {
                //Reverse the effect on our player
                boost.speed /= multiplier;
                bar.SetActive(false);

                //Remove power up object
                Instantiate(pickupEffect, player.transform.position, player.transform.rotation);
                Destroy(gameObject);

                break;
            }

            yield return new WaitForSeconds(1);
        }
    }


}